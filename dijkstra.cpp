/*
 * Задача 1. Города.
 * Требуется отыскать самый короткий маршрут между городами. Из города может выходить дорога, которая возвращается в этот же город.
 * Требуемое время работы O((N + M)log N), где N – количество городов, M – известных дорог между ними.
 * N ≤ 10000, M ≤ 250000. Длина каждой дороги ≤ 10000.
 * Память - O(N).
*/
#if defined HOME
#include "dijkstra.h"
#endif
#include <iostream>
#include <list>
#include <set>
#include <vector>
using std::list;
using std::pair;
using std::set;
using std::vector;
struct IGraph {
  virtual ~IGraph() {}

  virtual void AddEdge(int from, int to, int cost) = 0;

  virtual int VerticesCount() const = 0;

  virtual const std::vector<pair<int, int>>& GetNextVertices(
      int vertex) const = 0;
  virtual const std::vector<pair<int, int>>& GetPrevVertices(
      int vertex) const = 0;
};

struct ListGraph : public IGraph {
 private:
  std::vector<std::vector<pair<int, int>>> adjacency_list;
  std::vector<std::vector<pair<int, int>>> reverse_adjacency_list;

 public:
  ListGraph(int n) : adjacency_list(n), reverse_adjacency_list(n) {}
  ~ListGraph() = default;
  ListGraph(const IGraph* graph);
  void AddEdge(int from, int to, int cost) override;
  int VerticesCount() const override;
  const std::vector<pair<int, int>>& GetNextVertices(int vertex) const override;
  const std::vector<pair<int, int>>& GetPrevVertices(int vertex) const override;
};

int shortest_path(const IGraph& graph, int start, int end);

#if not defined NOTMAIN
int main() {
  int VerticesCount, EdgesCount;
  std::cin >> VerticesCount >> EdgesCount;
  ListGraph graph(VerticesCount);
  for (int i = 0; i < EdgesCount; i++) {
    int start, finish, cost;
    std::cin >> start >> finish >> cost;
    graph.AddEdge(start, finish, cost);
    graph.AddEdge(finish, start, cost);
  }
  int start, finish;
  std::cin >> start >> finish;
  std::cout << shortest_path(graph, start, finish);
  return 0;
}
#endif

int shortest_path(const IGraph& graph, int start, int end) {
  const int INF = 1000000000;
  vector<int> distance(graph.VerticesCount(), INF); 
  distance[start] = 0;
  set<pair<int, int>> prior_queue;
  prior_queue.insert({distance[start], start});
  while (!prior_queue.empty()) {
    int vertex = prior_queue.begin()->second;
    prior_queue.erase(prior_queue.begin());
    for (auto neigh : graph.GetNextVertices(vertex)) {
      int to = neigh.first, len = neigh.second;
      if (distance[vertex] + len < distance[to]) { // Релаксируем расстояния.
        prior_queue.erase({distance[to], to});
        distance[to] = distance[vertex] + len;
        prior_queue.insert({distance[to], to});
      }
    }
  }
  return distance[end];
}

void ListGraph::AddEdge(int from, int to, int cost) {
  ListGraph::adjacency_list[from].push_back({to, cost});
  ListGraph::reverse_adjacency_list[to].push_back({from, cost});
}

int ListGraph::VerticesCount() const {
  return ListGraph::adjacency_list.size();
}
const std::vector<pair<int, int>>& ListGraph::GetNextVertices(
    int vertex) const {
  return ListGraph::adjacency_list[vertex];
}

const std::vector<pair<int, int>>& ListGraph::GetPrevVertices(
    int vertex) const {
  return ListGraph::reverse_adjacency_list[vertex];
}

ListGraph::ListGraph(const IGraph* graph)
    : adjacency_list(graph->VerticesCount()) {
  vector<pair<int, int>> temp;
  for (int i = 0; i < adjacency_list.size(); i++) {
    temp.clear();
    temp = graph->GetNextVertices(i);
    adjacency_list[i].insert(adjacency_list[i].end(), temp.begin(), temp.end());
  }
}
